import numpy as np 
import pandas as pd 
import sklearn
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import OneHotEncoder
from sklearn.linear_model import LogisticRegression
from imblearn.over_sampling import SMOTE
import boto3
import dill
import joblib
from io import BytesIO
import json


df = pd.read_csv('/Users/meggievandenoever/Documents/Deeploy/marketing-campaign-ml/marketing_campaign.csv', sep='\t')

# split dataset
train_set, test_set = train_test_split(df, test_size=0.2, train_size=0.8, random_state=42, shuffle=True, stratify=df['Complain'])

#transform the dataset
# Year_Birth column 
train_set['Age'] = 2015 - train_set['Year_Birth']

train_set['Dt_Customer'] = pd.to_datetime(train_set['Dt_Customer'])
train_set['Dt_Customer'] = train_set['Dt_Customer'].dt.strftime('%Y-%m-%d')
train_set['Dt_Customer'] = pd.to_datetime(train_set['Dt_Customer'])

train_set['Date']= '2015-1-1'
train_set['Date'] = pd.to_datetime(train_set['Date'])
train_set['Days_Customer'] = (train_set['Date'] - train_set['Dt_Customer']).dt.days

train_set['Marital']=train_set['Marital_Status'].replace({'Married':'In Couple','Together':'In Couple','Single':'Single','Divorced':'Single','Widow':'Single','Alone':'Single','YOLO':'Single','Absurd':'Single'})

train_set['Marital']=train_set['Marital'].replace({'In Couple': 0, 'Single':1})

train_set['Education']=train_set['Education'].replace({'2n Cycle': 'Under grad', 'Basic':'Under grad', 'PhD':'Post grad', 'Master':'Post grad'})

education_cat=train_set[['Education']]

#print(train_set.info())

cat_encoder = OneHotEncoder()
education_cat_1hot = cat_encoder.fit_transform(education_cat)
education_cat_1hot2 = education_cat_1hot.toarray()

education_df = pd.DataFrame(education_cat_1hot2, columns=['Grad','Post grad','Under grad'])

education_df.reset_index(inplace=True)
train_set.reset_index(inplace=True)
train_set= pd.concat([train_set, education_df],axis=1)

train_set = train_set.drop(['index','Date'], axis=1)

kids_array = np.array([])
for element in range(0,1792):
  children = train_set.loc[element,'Kidhome'] + train_set.loc[element,'Teenhome']
  kids_array = np.append(kids_array, children)
kids_df = pd.DataFrame(kids_array, columns=['Kids'])
kids_df.reset_index(inplace=True)
train_set= pd.concat([train_set, kids_df],axis=1)
train_set = train_set.drop('index', axis=1)

spend_array = np.array([])
for element in range(0,1792):
  spend = train_set.loc[element,'MntWines'] + train_set.loc[element,'MntFruits'] + train_set.loc[element,'MntMeatProducts'] + train_set.loc[element,'MntFishProducts'] + train_set.loc[element,'MntSweetProducts'] + train_set.loc[element,'MntGoldProds']
  spend_array = np.append(spend_array, spend)
spend_df = pd.DataFrame(spend_array, columns=['MntSpend'])

spend_df.reset_index(inplace=True)
train_set= pd.concat([train_set, spend_df],axis=1)

#remove outlier
train_set = train_set.drop(414)

train_set['Income_cat'] = pd.cut(train_set['Income'],
                                       bins=[1700,56134 ,110568,165000],
                                       labels=['low income','average income','high income'])

train_set['Income_cat'] = train_set['Income_cat'].fillna('low income')

income_cat = train_set[['Income_cat']]
cat_encoder = OneHotEncoder()
income_cat_1hot = cat_encoder.fit_transform(income_cat)
income_cat_1hot2 = income_cat_1hot.toarray()

income_df = pd.DataFrame(income_cat_1hot2, columns=['average income', 'high income', 'low income'])
income_df.reset_index(inplace=True)
train_set.reset_index(inplace=True)

train_set= pd.concat([train_set, income_df],axis=1)

train_set = train_set.drop(['index','index'], axis=1)

train_set['Age_cat'] = pd.cut(train_set['Age'],
                                       bins=[16,35 ,55,130],
                                       labels=['young adult','middle aged','old adulthood'])
age_cat = train_set[['Age_cat']]

cat_encoder = OneHotEncoder()
age_cat_1hot = cat_encoder.fit_transform(age_cat)
age_cat_1hot2 = age_cat_1hot.toarray()

age_df = pd.DataFrame(age_cat_1hot2, columns=['middle aged', 'old adulthood', 'young adult'])
age_df.reset_index(inplace=True)
train_set.reset_index(inplace=True)
train_set= pd.concat([train_set, age_df],axis=1)

train_set, validation_set = train_test_split(train_set, test_size=0.2, random_state=42, shuffle=True, stratify=train_set['Complain'])

train_set = train_set.drop(['index','level_0'], axis=1)

columns1_df = train_set[['Response', 'Recency', 'MntSpend', 'NumWebPurchases','NumCatalogPurchases','NumStorePurchases','NumDealsPurchases','Grad','Post grad','Under grad','Kids','MntSpend','average income','high income','low income','middle aged','old adulthood','young adult']]
columns1 = ['Response','Recency', 'MntSpend', 'NumWebPurchases','NumCatalogPurchases','NumStorePurchases','NumDealsPurchases','Grad','Post grad','Under grad','Kids','MntSpend','average income','high income','low income','middle aged','old adulthood','young adult']

complain_labels = validation_set['Complain'].copy()

oversample = SMOTE()

train_set_smote = np.array(train_set[columns1])
train_set_smote, lables_smote = oversample.fit_resample(train_set_smote, train_set['Complain'].ravel())

train_set_smote = pd.DataFrame(train_set_smote, columns=columns1)
lables_smote = pd.DataFrame(lables_smote)

lables_smote['Complain']=lables_smote[0]
lables_smote = lables_smote.drop(columns=[0])

logRegrSmote = LogisticRegression()
logRegrSmote.fit(train_set_smote.values,lables_smote)

logRegr_pred = logRegrSmote.predict(validation_set.loc[:,columns1])

#score on validation set
#print(logRegrSmote.score(validation_set.loc[:,columns1], complain_labels))

bucket_model = 'mlmodelmeggie'
model_object_key = 'model.joblib'
s3 = boto3.client('s3')

def object_to_s3(bucket, key, object, object_type):
    buffer = BytesIO()
    if object_type == 'explainer':
        dill.dump(object, buffer)
    else:
        joblib.dump(object, buffer)
    buffer.seek(0)
    sent_buffer = s3.put_object(Bucket=bucket, Key=key, Body=buffer)
    return sent_buffer['ResponseMetadata']

object_to_s3(bucket_model, model_object_key, logRegrSmote, 'model')

# Add reference to repo
model_reference = {
    'reference': {
        'blob': {
            'url': "s3://mlmodelmeggie/model"
            }
        }
    }

with open('reference.json', 'w', encoding='utf-8') as f:
    json.dump(model_reference, f, ensure_ascii=False, indent=4)